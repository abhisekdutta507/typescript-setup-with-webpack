const path = require('path');
module.exports = {
  entry: './src/main.ts',
  mode: 'production',
  module: {
    rules: [
      { test: /\.ts$/, use: 'ts-loader' },
    ]
  },
  resolve: {
    extensions: [
      '.ts'
    ]
  },
  watch: true,
  watchOptions: {
    ignored: /node_modules/
  },
  output: {
    filename: 'bundle.js',
    path: path.resolve(__dirname, './build'),
  },
  devServer: {
    contentBase: path.join(__dirname, './build'),
    compress: true,
    port: 9000
  }
};
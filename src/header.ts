import { CSSStyleProps } from './interfaces/cssStyleProps';

import { Toolbar } from './toolbar';
import { Searchbar } from './searchbar';

import { tween, styler, easing } from 'popmotion';

let that: any;

/**
 * @description Header class should be able to deal with the Header HTMLElement in the DOM.
 * ES6 class declaration.
 */
export class Header {
  item: HTMLElement;

  // sb is Search button.
  sb: HTMLElement;
  // csb is Close search button.
  csb: HTMLElement;

  selector: string;
  sbs: string;
  csbs: string;

  private toolbar: Toolbar;
  private searchbar: Searchbar;

  constructor(s: string) {
    this.selector = s;
    this.sbs = '.fa-search';
    this.csbs = '.fa-arrow-left';

    this.item = document.querySelector(s);
    this.sb = this.item.querySelector(this.sbs);
    this.csb = this.item.querySelector(this.csbs);
    
    this.toolbar = new Toolbar('#toolbar');
    this.searchbar = new Searchbar('#searchbar');

    that = this;
  }

  /**
   * @description onLoad is executed when the DOM is loaded.
   * ES6 function declaration.
   */
  onLoad = async () => {
    this.sb.addEventListener('click', this.onSearchClick);
    this.csb.addEventListener('click', this.onCloseSearchClick);

    this.searchbar.onLoad();
  }

  onSearchClick(e: Event) {
    tween({
      from: { x: 0, scale: 1 },
      to: { x: -that.toolbar.item.clientWidth, scale: 1 },
      ease: easing.anticipate,
      duration: 1000
    }).start({
      update: (v: any) => that.toolbar.pmitem.set(v),
      complete: () => {}
    });

    tween({
      from: { x: that.searchbar.item.clientWidth, scale: 1 },
      to: { x: 0, scale: 1 },
      ease: easing.anticipate,
      duration: 1000
    }).start({
      update: (v: any) => that.searchbar.pmitem.set(v),
      complete: () => {}
    });
  }

  onCloseSearchClick(e: Event) {
    tween({
      from: { x: -that.toolbar.item.clientWidth, scale: 1 },
      to: { x: 0, scale: 1 },
      ease: easing.anticipate,
      duration: 1000
    }).start({
      update: (v: any) => that.toolbar.pmitem.set(v),
      complete: () => {}
    });

    tween({
      from: { x: 0, scale: 1 },
      to: { x: that.searchbar.item.clientWidth, scale: 1 },
      ease: easing.anticipate,
      duration: 1000
    }).start({
      update: (v: any) => that.searchbar.pmitem.set(v),
      complete: () => {}
    });
  }

  /**
   * @description setStyle can update the inline style property on the HTMLElement.
   * ES6 function declaration.
   * @param s is of CSSStyleProps type. ES6 default assigned value is {}.
   */
  setStyle = (s: CSSStyleProps = {}) => {
    Object.assign(this.item.style, s);
  }

}
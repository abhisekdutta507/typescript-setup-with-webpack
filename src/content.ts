// Import components here
import { Nodata } from './components/nodata';
import { Contact } from './components/contact';
import { Wrapper } from './components/wrapper';

import { CSSStyleProps } from './interfaces/cssStyleProps';
import { ContactDetails } from './interfaces/contact';
/**
 * @description Content class should be able to deal with the Content HTMLElement in the DOM.
 * ES6 class declaration.
 */
export class Content {
  private wrapper: Wrapper;
  item: HTMLElement;
  selector: string;
  
  constructor(s: string) {
    this.wrapper = new Wrapper();
    this.selector = s;
    this.item = document.querySelector(s);
  }

  /**
   * @description setStyle can update the inline style property on the HTMLElement.
   * ES6 function declaration.
   * @param s is of CSSStyleProps type. ES6 default assigned value is {}.
   */
  setStyle(s: CSSStyleProps = {}) {
    Object.assign(this.item.style, s);
  }

  /**
   * @description generateContactsDOM is used to generate the DOM from the contacts array.
   * ES6 function declaration.
   */
  generateContactsDOM = async (contactList: Array<ContactDetails>) => {
    // Generate the DOM
    let contacts: HTMLElement[];
    contacts = contactList.length
    ? this.wrapper.wrapEach(contactList.map(it => new Contact('flex-container contact p-v-10', it).item), 'li', 'contact-wrapper')
    : this.wrapper.wrapEach(['No data found!'].map(it => new Nodata(it).item), 'li', 'contact-wrapper')

    // Append the DOM inside the content
    this.item.innerHTML = '';
    this.item.appendChild(this.wrapper.wrapBy(contacts, 'ul', 'contacts'));
  }

}
// Import wrapper components here
import { App } from './app';
import { Toolbar } from './toolbar';
import { Header } from './header';
import { Content } from './content';

// Import serveices here
import { Http } from './services/http';

// Import interfaces here
import { ContactDetails } from './interfaces/contact';
import { HttpResponse } from './interfaces/httpResponse';

let that: any;

/**
 * @description Main class has the ability to manipulate the DOM.
 * ES6 class declaration.
 */
class Main {
  private header: Header;
  private toolbar: Toolbar;
  private content: Content;
  private app: App;
  private http: Http;
  private contactList: Array<ContactDetails>;

  constructor() {
    this.header = new Header('#header');
    this.toolbar = new Toolbar('#toolbar');
    this.content = new Content('#content');
    this.app = new App('#app');
    this.http = new Http();

    that = this;
  }

  /**
   * @description onLoad is executed when the DOM is loaded.
   * ES6 function declaration.
   */
  onLoad = async () => {
    that.fixContentMargin();
    that.users();

    that.header.onLoad();
  }

  /**
   * @description fixContentMargin can fix the content topMargin dynamically.
   * ES6 function declaration.
   */
  fixContentMargin = async () => {
    this.content.setStyle({
      marginTop: `${this.toolbar.item.clientHeight}px`
    });
  }

  /**
   * @description users is used to get the contacts from NodeAPIs server.
   * ES6 function declaration.
   */
  users = async () => {
    let r: HttpResponse = await this.http.users('crudapp/users?limit=50');
    this.contactList = r.json.data || [];

    // Generate the DOM when contacts are available.
    this.content.generateContactsDOM(this.contactList);
  }
}

/**
 * @description pro is an object of the Main class.
 * ES6 object creation.
 */
const pro = new Main();

/**
 * @description Binding the onLoad function with a DOM load event. So that, 
 * when the DOM is loaded the onLoad function will be executed.
 */
document.addEventListener('DOMContentLoaded', pro.onLoad);
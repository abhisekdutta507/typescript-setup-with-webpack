export interface ContactDetails {
  photoPath?: string;
  name?: string;
  phoneNumber?: string;
  email?: string;
  gender?: string;

  contactPreference?: string;
  dateOfBirth?: string;
  department?: string;
  isActive?: boolean;
  __v?: number;
  _id?: string;
};
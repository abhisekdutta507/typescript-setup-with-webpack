export interface HttpResponse {
  json?: any;
  state?: number;
  status?: number;
  string?: string;
  text?: string;
  timeout?: number;
  url?: string;
};
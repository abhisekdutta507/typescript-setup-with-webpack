import { CSSStyleProps } from './interfaces/cssStyleProps';
import { tween, styler, easing } from 'popmotion';

// Import wrapper components here
import { Content } from './content';

// Import serveices here
import { Http } from './services/http';

// Import interfaces here
import { ContactDetails } from './interfaces/contact';
import { HttpResponse } from './interfaces/httpResponse';

let that: any;

/**
 * @description Searchbar class should be able to deal with the Searchbar HTMLElement in the DOM.
 * ES6 class declaration.
 */
export class Searchbar {
  item: HTMLElement;
  pmitem: any;

  // ti is Text input.
  ti: HTMLElement;

  selector: string;
  tis: string;

  private http: Http;
  private content: Content;
  private contactList: Array<ContactDetails>;

  // sd is search delay.
  private sd: number;

  constructor(s: string) {
    this.tis = '#search-text';
    this.selector = s;

    this.content = new Content('#content');
    this.item = document.querySelector(s);
    this.ti = this.item.querySelector(this.tis);

    this.http = new Http();

    this.pmitem = styler(this.item);
    this.pmitem.set({ x: this.item.clientWidth, scale: 0 });

    that = this;
  }

  onLoad() {
    this.ti.addEventListener('keyup', this.onTextSearch);
  }

  onTextSearch(e: any) {
    clearTimeout(that.sd);

    that.sd = setTimeout(() => {
      that.users(e.target.value);
    }, 400);
  }

  /**
   * @description users is used to get the contacts from NodeAPIs server.
   * ES6 function declaration.
   */
  users = async (key: string) => {
    let r: HttpResponse = await this.http.users(`crudapp/users?search=${key}&limit=50`);
    this.contactList = r.json.data || [];

    // Generate the DOM when contacts are available.
    this.content.generateContactsDOM(this.contactList);
  }

  /**
   * @description setStyle can update the inline style property on the HTMLElement.
   * ES6 function declaration.
   * @param s is of CSSStyleProps type. ES6 default assigned value is {}.
   */
  setStyle(s: CSSStyleProps = {}) {
    Object.assign(this.item.style, s);
  }

}
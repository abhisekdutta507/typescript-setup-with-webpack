import { CSSStyleProps } from '../interfaces/cssStyleProps';
import { ContactDetails } from '../interfaces/contact';

export class Contact {
  item: HTMLElement;
  selector: string;

  constructor(s: string, option?: ContactDetails) {
    this.selector = s;

    this.item = Object.assign(document.createElement('div'), {
      className: s,
      innerHTML: 
        `<div class="flex m-a-v">
          <!-- <p class="heading p-b-3">${option.photoPath}</p> -->
          <i class="fas fa-user"></i>
        </div>

        <div class="flex m-a-v p-l-10">
          <p class="heading p-b-3">${option.name}</p>
          <p class="sub-heading">${option.phoneNumber || option.email}</p>
        </div>`
    });
  }

  setStyle(s: CSSStyleProps = {}) {
    Object.assign(this.item.style, s);
  }

}
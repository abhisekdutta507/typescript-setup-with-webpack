import { CSSStyleProps } from '../interfaces/cssStyleProps';

export class Nodata {
  item: HTMLElement;
  selector: string;

  constructor(t: string, s?: string) {
    this.selector = s || 'no-data';

    this.item = Object.assign(document.createElement('div'), {
      className: this.selector,
      innerHTML: 
        `<div class="flex m-a-v">
          <p class="sub-heading">${t}</p>
        </div>`
    });
  }

  setStyle(s: CSSStyleProps = {}) {
    Object.assign(this.item.style, s);
  }

}
export class Wrapper {
  wrapBy(items: HTMLElement[], tag: string, selector?: string): HTMLElement {
    let el: HTMLElement = Object.assign(document.createElement(tag), {className: selector || ''});
    items.forEach(it => { el.appendChild(it); });
    return el;
  }

  wrapEach(items: HTMLElement[], tag: string, selector?: string): HTMLElement[] {
    let es: HTMLElement[] = items.map(it => { let el: HTMLElement = Object.assign(document.createElement(tag), {className: selector || ''}); el.appendChild(it); return el; });
    return es;
  }

}
let that: any;

export class Http {
  private baseUrl: string;

  constructor(
  ) {
    that = this;
    this.baseUrl = 'https://nodeapis101.herokuapp.com/api/v1/';
  }

  print = () => {
    console.log('execute the print call');
  }

  r200 = (r: any) => Object.assign({
    timeout: r.timeout,
    status: r.status,
    text: r.statusText,
    state: r.readyState,
    url: r.responseURL,
    string: r.responseText,
    json: JSON.parse(r.response)
  });

  r404 = (r: any) => Object.assign({
    timeout: r.timeout,
    status: r.status,
    text: r.statusText,
    state: r.readyState,
    url: r.responseURL,
    string: r.responseText,
    json: { message: JSON.parse(r.response) }
  });

  users = async (url: string) => {
    return new Promise((next, error) => {
      let xhttp = new XMLHttpRequest();
      xhttp.onreadystatechange = function() {
        if (this.readyState === 4) { //  && this.status === 200
          switch(this.status) {
            case 200: next(that.r200(this));
            case 404: next(that.r404(this));
          }
        }
      };

      xhttp.open('GET', `${this.baseUrl}${url}`, true);
      xhttp.send();
    });
  }
}
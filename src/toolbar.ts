import { CSSStyleProps } from './interfaces/cssStyleProps';
import { tween, styler, easing } from 'popmotion';

/**
 * @description Toolbar class should be able to deal with the Toolbar HTMLElement in the DOM.
 * ES6 class declaration.
 */
export class Toolbar {
  item: HTMLElement;
  pmitem: any;

  selector: string;

  constructor(s: string) {
    this.selector = s;
    this.item = document.querySelector(s);

    this.pmitem = styler(this.item);
    this.pmitem.set({ x: 0, scale: 1 });
  }

  /**
   * @description setStyle can update the inline style property on the HTMLElement.
   * ES6 function declaration.
   * @param s is of CSSStyleProps type. ES6 default assigned value is {}.
   */
  setStyle(s: CSSStyleProps = {}) {
    Object.assign(this.item.style, s);
  }

}
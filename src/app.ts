import { CSSStyleProps } from './interfaces/cssStyleProps';

/**
 * @description App class should be able to deal with the App HTMLElement in the DOM.
 * ES6 class declaration.
 */
export class App {
  item: HTMLElement;
  selector: string;

  constructor(s: string) {
    this.selector = s;
    this.item = document.querySelector(s);
  }

  /**
   * @description setStyle can update the inline style property on the HTMLElement.
   * ES6 function declaration.
   * @param s is of CSSStyleProps type. ES6 default assigned value is {}.
   */
  setStyle = (s: CSSStyleProps = {}) => {
    Object.assign(this.item.style, s);
  }

}
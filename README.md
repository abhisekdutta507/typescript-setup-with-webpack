This project was bootstrapped with `npm init`.
We have added typescript support to the project using [typescript](https://code.visualstudio.com/docs/typescript/typescript-compiling) with [webpack-cli](https://webpack.js.org/guides/installation/).

Press  `Cmd + Shift + V`  form your Mac keyboard to preview the README.md.

Go to https://fervent-jones-fba2d0.netlify.com/ for the live demo.

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br />
Then, drag & drop the `./build/index.html` file in a browser window to run the application.

## Learn More

You can learn more in the [Webpack CLI Documentation](https://webpack.js.org/guides/installation/).

### Simple TypeScript Setup With TypeScript Compiler & Webpack CLI

**Initialize the project with `NPM`.** *Make sure you have a stable version of* [NodeJS](https://nodejs.org/en/), [NPM](https://www.npmjs.com/) & [Webpack CLI](https://webpack.js.org/guides/installation/) *is installed in your system globally.*

```bash
npm init
```

Answer some questions asked by the `Node Package Manager (npm)` when initiating the new project.

**Create a** `tsconfig.json` **file in your project root.**

Create it from terminal & update with the below example,
```bash
tsc --init
```

Or simply add the below codes:
```json
{
  "compilerOptions": {
    "target": "es6",
    "module": "commonjs",
    "sourceMap": true,
    "experimentalDecorators": true,
    "allowJs": false,
    "outDir": "build"
  },

  "include": [
    "src/*.ts",
    "src/*/*.ts",
  ],

  "exclude": [
    "node_modules",
    "build/*.js"
  ],

  "compileOnSave": true
}
```

**Here, add a** `webpack.config.js` **file in your project root.**

Add the below codes:

```js
const path = require('path');
module.exports = {
  entry: './src/main.ts',
  mode: 'production',
  module: {
    rules: [
      { test: /\.ts$/, use: 'ts-loader' },
    ]
  },
  resolve: {
    extensions: [
      '.ts'
    ]
  },
  watch: true,
  watchOptions: {
    ignored: /node_modules/
  },
  output: {
    filename: 'bundle.js',
    path: path.resolve(__dirname, './build'),
  }
};
```

**Now, add the developer dependencies to your project.**

From the project root run:

```bash
sudo npm i typescript ts-loader webpack webpack-cli --save-dev
```

**The final step is to update the `package.json`.**

```json
{
  "scripts": {
    "start": "webpack-cli",

    ...
  },

  ...
}
```

**Hurray!! we are ready to go now.**

Run the app in the development mode.

Create `src` folder in the project root. And add the `main.ts` file inside the `src` folder.

Start compiling the project with:

```bash
npm start
```

It generates `./build/bundle.js` file in your project root. We can embed this `bundle.js` in our HTML, CSS project to start working.